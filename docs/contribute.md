# Contribuir a la wiki

Aquesta wiki es en realitat viu a un repositori Git a [gitlab.com](https://gitlab.com). Per accedir als arxius
de la wiki, seguir el següent enllaç:

[Accés als arxius de la wiki](https://gitlab.com/openmemetro/wiki/-/tree/main/docs)

Si no saps que significa no et preocupis! A continuació t'expliquem com col·laborar a la wiki!

### 1. Fer-te una conta

Primer de tot necessitarem una conta a [gitlab.com](https://gitlab.com). Accedirem a aquest enllaç 
i crearem una conta com a qualsevol altre lloc web:

https://gitlab.com/users/sign_up/

La conta es necessària per poder editar un arxiu.

### 2. Afegir informació a un arxiu existent

#### - Cercar l'arxiu

Un cop tenim la conta i estem "loguejades", podem modificar una arxiu en concret, per exemple, si volem
afegir informació a la parada de [Fontana](stations/fontana.md), hi accedirem i polsarem al text `Edita`, 
a la part superior de la pàgina d'aquesta mateixa wiki. 

> També podem accedir a l'arxiu des de la pàgina de gitlab, buscant la carpeta `stations` i cercant l'arxiu `fontana.md`. 
També podem buscar per nom del arxiu al botó `Find file` a la part superior de la web. 

Un cop obert l'arxiu, el podrem editar fent servir la fletxeta del botó blau gros de la part superior i 
seleccionarem `Edit`.

![GitHub Pages](files/contribute_edit_btn.png)

Llavors ens avisarà de que hem de fer un `Fork` per poder editar l'arxiu, l'hi donarem clic a `Fork`.

#### - Modificar l'arxiu

Al obrir-se l'editor simple podrem fer canvis a l'arxiu i proposar-los fent servir el botó blau `Commit changes` a 
la part inferior de la pàgina. 

Per crear titols, links, llistes, etc, es fa servir llenguatge `Markdown`, podem veure com ferlo servir 
[aquí](https://gitlab.com/help/user/markdown)

Podem veure una vista prèvia de com queden els canvis a la pestanya `Preview` de l'editor. 

Un cop pressionat el botó `Commit changes` arribara un missatge als administradors de la wiki i passarà a 
l'espera d'aprovació. 


## Veure l'historial de canvis

Podem veure l'historial de canvis d'un arxiu i quin usuari ho ha fet accedint a l'historial de l'arxiu a gitlab.

