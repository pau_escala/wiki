
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia L4

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Metro](/transports/metro.md)

## Estacions

* [Urquinaona](/stations/urquinaona.md)
* [Pg. Gràcia](/stations/pg-gracia.md)
* [La Pau](/stations/la-pau.md)
* [Trinitat Nova](/stations/trinitat-nova.md)
* [Besòs](/stations/besos.md)
* [Besòs Mar](/stations/besos-mar.md)
* [El Maresme | Fòrum](/stations/el-maresme-forum.md)
* [Selva de Mar](/stations/selva-de-mar.md)
* [Poblenou](/stations/poblenou.md)
* [Llacuna](/stations/llacuna.md)
* [Bogatell](/stations/bogatell.md)
* [Ciutadella | Vila Olímpica](/stations/ciutadella-vila-olimpica.md)
* [Barceloneta](/stations/barceloneta.md)
* [Jaume I](/stations/jaume-i.md)
* [Girona](/stations/girona.md)
* [Verdaguer](/stations/verdaguer.md)
* [Joanic](/stations/joanic.md)
* [Alfons X](/stations/alfons-x.md)
* [Guinardó | Hospital de St. Pau](/stations/guinardo-hospital-de-st-pau.md)
* [Maragall](/stations/maragall.md)
* [Llucmajor](/stations/llucmajor.md)
* [Via Júlia](/stations/via-julia.md)
