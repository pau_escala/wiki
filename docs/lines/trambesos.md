
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia Trambesòs

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Tram](/transports/tram.md)

## Estacions

* [Ciutadella | Vila Olímpica](/stations/ciutadella-vila-olimpica.md)
* [Wellington](/stations/wellington.md)
* [Marina](/stations/marina.md)
* [Auditori | Teatre Nacional](/stations/auditori-teatre-nacional.md)
* [Glòries](/stations/glories.md)
* [Ca l'Aranyó](/stations/ca-laranyo.md)
* [Pere IV](/stations/pere-iv.md)
* [Fluvià](/stations/fluvia.md)
* [Selva de Mar](/stations/selva-de-mar.md)
* [El Maresme](/stations/el-maresme.md)
* [Fòrum](/stations/forum.md)
* [Campus Diagonal-Besòs](/stations/campus-diagonal-besos.md)
* [Port Fòrum](/stations/port-forum.md)
* [Est. St. Adrià](/stations/est-st-adria.md)
* [La Farinera](/stations/la-farinera.md)
* [Can Jaumandreu](/stations/can-jaumandreu.md)
* [Espronceda](/stations/espronceda.md)
* [St. Martí de Provençals](/stations/st-marti-de-provencals.md)
* [Besòs](/stations/besos.md)
* [Alfons el Magnànim](/stations/alfons-el-magnanim.md)
* [Parc del Besòs](/stations/parc-del-besos.md)
* [La Catalana](/stations/la-catalana.md)
* [St. Joan Baptista](/stations/st-joan-baptista.md)
* [Encants de St. Adrià](/stations/encants-de-st-adria.md)
* [St. Roc](/stations/st-roc.md)
* [Gorg](/stations/gorg.md)
* [La Mina](/stations/la-mina.md)
