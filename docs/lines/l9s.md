
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia L9S

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Metro](/transports/metro.md)

## Estacions

* [Torrassa](/stations/torrassa.md)
* [Zona Universitària](/stations/zona-universitaria.md)
* [Collblanc](/stations/collblanc.md)
* [Aeroport T1](/stations/aeroport-t1.md)
* [Aeroport T2](/stations/aeroport-t2.md)
* [Mas Blau](/stations/mas-blau.md)
* [Parc Nou](/stations/parc-nou.md)
* [Cèntric](/stations/centric.md)
* [El Prat Estació](/stations/el-prat-estacio.md)
* [Les Moreres](/stations/les-moreres.md)
* [Mercabarna](/stations/mercabarna.md)
* [Parc Logístic](/stations/parc-logistic.md)
* [Fira](/stations/fira.md)
* [Europa | Fira](/stations/europa-fira.md)
* [Can Tries | Gornal](/stations/can-tries-gornal.md)
