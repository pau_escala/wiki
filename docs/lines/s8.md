
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia S8

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [FGC](/transports/fgc.md)

## Estacions

* [BCN-Pl. Espanya](/stations/bcn-pl-espanya.md)
* [Magòria La Campana](/stations/magoria-la-campana.md)
* [Ildefons Cerdà](/stations/ildefons-cerda.md)
* [Europa | Fira](/stations/europa-fira.md)
* [Gornal](/stations/gornal.md)
* [St. Josep](/stations/st-josep.md)
* [L'Hospitalet Av. Carrilet](/stations/lhospitalet-av-carrilet.md)
* [Almeda](/stations/almeda.md)
* [Cornellà Riera](/stations/cornella-riera.md)
* [St. Boi](/stations/st-boi.md)
* [Molí Nou | Ciutat Cooperativa](/stations/moli-nou-ciutat-cooperativa.md)
* [Colònia Güell](/stations/colonia-guell.md)
* [Sta. Coloma de Cervelló](/stations/sta-coloma-de-cervello.md)
* [St. Vicenç dels Horts](/stations/st-vicenc-dels-horts.md)
* [Can Ros](/stations/can-ros.md)
* [Quatre Camins](/stations/quatre-camins.md)
* [Pallejà](/stations/palleja.md)
* [St. Andreu de la Barca](/stations/st-andreu-de-la-barca.md)
* [El Palau](/stations/el-palau.md)
* [Martorell Vila | Castellbisbal](/stations/martorell-vila-castellbisbal.md)
* [Martorell Central](/stations/martorell-central.md)
* [Martorell Enllaç](/stations/martorell-enllac.md)
