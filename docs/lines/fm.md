
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia FM

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Metro](/transports/metro.md)

## Estacions

* [Paral·lel](/stations/parallel.md)
* [Parc de Montjuïc](/stations/parc-de-montjuic.md)
