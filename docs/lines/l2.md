
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia L2

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Metro](/transports/metro.md)

## Estacions

* [Universitat](/stations/universitat.md)
* [Clot](/stations/clot.md)
* [Paral·lel](/stations/parallel.md)
* [St. Antoni](/stations/st-antoni.md)
* [Pg. Gràcia](/stations/pg-gracia.md)
* [Tetuan](/stations/tetuan.md)
* [Monumental](/stations/monumental.md)
* [Sagrada Família](/stations/sagrada-familia.md)
* [Encants](/stations/encants.md)
* [Bac de Roda](/stations/bac-de-roda.md)
* [St. Martí](/stations/st-marti.md)
* [La Pau](/stations/la-pau.md)
* [Verneda](/stations/verneda.md)
* [Artigues | St. Adrià](/stations/artigues-st-adria.md)
* [St. Roc](/stations/st-roc.md)
* [Gorg](/stations/gorg.md)
* [Pep Ventura](/stations/pep-ventura.md)
* [Badalona Pompeu Fabra](/stations/badalona-pompeu-fabra.md)
