
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia R2S

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Renfe](/transports/renfe.md)

## Estacions

* [St. Vicenç de Calders](/stations/st-vicenc-de-calders.md)
* [Calafell](/stations/calafell.md)
* [Segur de Calafell](/stations/segur-de-calafell.md)
* [Cunit](/stations/cunit.md)
* [Cubelles](/stations/cubelles.md)
* [Vilanova i la Geltrú](/stations/vilanova-i-la-geltru.md)
* [Sitges](/stations/sitges.md)
* [Garraf](/stations/garraf.md)
* [Platja de Castelldefels](/stations/platja-de-castelldefels.md)
* [Castelldefels](/stations/castelldefels.md)
* [Gavà](/stations/gava.md)
* [Viladecans](/stations/viladecans.md)
* [El Prat de Llobregat](/stations/el-prat-de-llobregat.md)
* [Bellvitge](/stations/bellvitge.md)
* [BCN-Sants](/stations/bcn-sants.md)
* [BCN-Pg. Gràcia](/stations/bcn-pg-gracia.md)
* [BCN-Est. França](/stations/bcn-est-franca.md)
