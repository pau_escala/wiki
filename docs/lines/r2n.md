
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia R2N

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Renfe](/transports/renfe.md)

## Estacions

* [Aeroport](/stations/aeroport.md)
* [El Prat de Llobregat](/stations/el-prat-de-llobregat.md)
* [Bellvitge](/stations/bellvitge.md)
* [BCN-Sants](/stations/bcn-sants.md)
* [BCN-Pg. Gràcia](/stations/bcn-pg-gracia.md)
* [BCN-El Clot-Aragó](/stations/bcn-el-clot-arago.md)
* [BCN-St. Andreu Comtal](/stations/bcn-st-andreu-comtal.md)
* [Montcada i Reixac](/stations/montcada-i-reixac.md)
* [La Llagosta](/stations/la-llagosta.md)
* [Mollet-St. Fost](/stations/mollet-st-fost.md)
* [Montmeló](/stations/montmelo.md)
* [Granollers Centre](/stations/granollers-centre.md)
* [Les Franqueses-Granollers Nord](/stations/les-franqueses-granollers-nord.md)
* [Cardedeu](/stations/cardedeu.md)
* [Llinars del Vallès](/stations/llinars-del-valles.md)
* [Palautordera](/stations/palautordera.md)
* [St. Celoni](/stations/st-celoni.md)
* [Gualba](/stations/gualba.md)
* [Riells i Viabrea-Breda](/stations/riells-i-viabrea-breda.md)
* [Hostalric](/stations/hostalric.md)
* [Maçanet-Massanes](/stations/macanet-massanes.md)
