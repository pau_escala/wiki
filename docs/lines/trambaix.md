
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia Trambaix

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Tram](/transports/tram.md)

## Estacions

* [Francesc Macià](/stations/francesc-macia.md)
* [L'Illa](/stations/lilla.md)
* [Numància](/stations/numancia.md)
* [Maria Cristina](/stations/maria-cristina.md)
* [Pius XII](/stations/pius-xii.md)
* [Palau Reial](/stations/palau-reial.md)
* [Zona Universitària](/stations/zona-universitaria.md)
* [Avinguda de Xile](/stations/avinguda-de-xile.md)
* [Ernest Lluch](/stations/ernest-lluch.md)
* [Can Rigal](/stations/can-rigal.md)
* [Ca n'Oliveres](/stations/ca-noliveres.md)
* [Can Clota](/stations/can-clota.md)
* [Pont d'Esplugues](/stations/pont-desplugues.md)
* [La Sardana](/stations/la-sardana.md)
* [Montesa](/stations/montesa.md)
* [El Pedró](/stations/el-pedro.md)
* [Ignasi Iglésias](/stations/ignasi-iglesias.md)
* [Cornellà Centre](/stations/cornella-centre.md)
* [Les Aigües](/stations/les-aigues.md)
* [Fontsanta | Fatjó](/stations/fontsanta-fatjo.md)
* [Bon Viatge](/stations/bon-viatge.md)
* [La Fontsanta](/stations/la-fontsanta.md)
* [Centre Miquel Martí i Pol](/stations/centre-miquel-marti-i-pol.md)
* [Baixador](/stations/baixador.md)
* [Hospital de St. Joan Despí | TV](/stations/hospital-de-st-joan-despi-tv.md)
* [Rambla de St. Just](/stations/rambla-de-st-just.md)
* [Walden](/stations/walden.md)
* [Torreblanca](/stations/torreblanca.md)
* [St. Feliu | Consell Comarcal](/stations/st-feliu-consell-comarcal.md)
