
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Línia L9N

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

* [Metro](/transports/metro.md)

## Estacions

* [La Sagrera](/stations/la-sagrera.md)
* [Fondo](/stations/fondo.md)
* [Onze de Setembre](/stations/onze-de-setembre.md)
* [Bon Pastor](/stations/bon-pastor.md)
* [Can Peixauet](/stations/can-peixauet.md)
* [Sta. Rosa](/stations/sta-rosa.md)
* [Església Major](/stations/esglesia-major.md)
* [Singuerlín](/stations/singuerlin.md)
* [Can Zam](/stations/can-zam.md)
