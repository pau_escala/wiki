
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Tram

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 
    
## Línies
    
* [Trambaix](/lines/trambaix.md)
* [Trambesòs](/lines/trambesos.md)
    
## Estacions
    
* [Francesc Macià](/stations/francesc-macia.md)
* [L'Illa](/stations/lilla.md)
* [Numància](/stations/numancia.md)
* [Maria Cristina](/stations/maria-cristina.md)
* [Pius XII](/stations/pius-xii.md)
* [Palau Reial](/stations/palau-reial.md)
* [Zona Universitària](/stations/zona-universitaria.md)
* [Avinguda de Xile](/stations/avinguda-de-xile.md)
* [Ernest Lluch](/stations/ernest-lluch.md)
* [Can Rigal](/stations/can-rigal.md)
* [Ca n'Oliveres](/stations/ca-noliveres.md)
* [Can Clota](/stations/can-clota.md)
* [Pont d'Esplugues](/stations/pont-desplugues.md)
* [La Sardana](/stations/la-sardana.md)
* [Montesa](/stations/montesa.md)
* [El Pedró](/stations/el-pedro.md)
* [Ignasi Iglésias](/stations/ignasi-iglesias.md)
* [Cornellà Centre](/stations/cornella-centre.md)
* [Les Aigües](/stations/les-aigues.md)
* [Fontsanta | Fatjó](/stations/fontsanta-fatjo.md)
* [Bon Viatge](/stations/bon-viatge.md)
* [La Fontsanta](/stations/la-fontsanta.md)
* [Centre Miquel Martí i Pol](/stations/centre-miquel-marti-i-pol.md)
* [Baixador](/stations/baixador.md)
* [Hospital de St. Joan Despí | TV](/stations/hospital-de-st-joan-despi-tv.md)
* [Rambla de St. Just](/stations/rambla-de-st-just.md)
* [Walden](/stations/walden.md)
* [Torreblanca](/stations/torreblanca.md)
* [St. Feliu | Consell Comarcal](/stations/st-feliu-consell-comarcal.md)
* [Ciutadella | Vila Olímpica](/stations/ciutadella-vila-olimpica.md)
* [Wellington](/stations/wellington.md)
* [Marina](/stations/marina.md)
* [Auditori | Teatre Nacional](/stations/auditori-teatre-nacional.md)
* [Glòries](/stations/glories.md)
* [Ca l'Aranyó](/stations/ca-laranyo.md)
* [Pere IV](/stations/pere-iv.md)
* [Fluvià](/stations/fluvia.md)
* [Selva de Mar](/stations/selva-de-mar.md)
* [El Maresme](/stations/el-maresme.md)
* [Fòrum](/stations/forum.md)
* [Campus Diagonal-Besòs](/stations/campus-diagonal-besos.md)
* [Port Fòrum](/stations/port-forum.md)
* [Est. St. Adrià](/stations/est-st-adria.md)
* [La Farinera](/stations/la-farinera.md)
* [Can Jaumandreu](/stations/can-jaumandreu.md)
* [Espronceda](/stations/espronceda.md)
* [St. Martí de Provençals](/stations/st-marti-de-provencals.md)
* [Besòs](/stations/besos.md)
* [Alfons el Magnànim](/stations/alfons-el-magnanim.md)
* [Parc del Besòs](/stations/parc-del-besos.md)
* [La Catalana](/stations/la-catalana.md)
* [St. Joan Baptista](/stations/st-joan-baptista.md)
* [Encants de St. Adrià](/stations/encants-de-st-adria.md)
* [St. Roc](/stations/st-roc.md)
* [Gorg](/stations/gorg.md)
* [La Mina](/stations/la-mina.md)
