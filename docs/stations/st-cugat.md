
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# St. Cugat

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [FGC](/transports/fgc.md)

## Línies
    
* [S1](/lines/s1.md)
* [S2](/lines/s2.md)
* [S5](/lines/s5.md)
* [S6](/lines/s6.md)
* [S7](/lines/s7.md)
