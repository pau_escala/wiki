
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# El Maresme

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Tram](/transports/tram.md)

## Línies
    
* [Trambesòs](/lines/trambesos.md)
