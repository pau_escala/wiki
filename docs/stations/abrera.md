
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Abrera

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [FGC](/transports/fgc.md)

## Línies
    
* [S4](/lines/s4.md)
* [R5](/lines/r5.md)
* [R50](/lines/r50.md)
