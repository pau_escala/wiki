
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Gorg

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Tram](/transports/tram.md)

## Línies
    
* [L1](/lines/l1.md)
* [L2](/lines/l2.md)
* [L10N](/lines/l10n.md)
* [Trambesòs](/lines/trambesos.md)
