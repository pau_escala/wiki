
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Zona Universitària

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Tram](/transports/tram.md)

## Línies
    
* [L3](/lines/l3.md)
* [L9S](/lines/l9s.md)
* [Trambaix](/lines/trambaix.md)
