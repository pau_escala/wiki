
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Tarragona

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Metro](/transports/metro.md)

## Línies
    
* [R14](/lines/r14.md)
* [R15](/lines/r15.md)
* [R16](/lines/r16.md)
* [L3](/lines/l3.md)
