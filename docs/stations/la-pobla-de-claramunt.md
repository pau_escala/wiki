
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# La Pobla de Claramunt

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [FGC](/transports/fgc.md)

## Línies
    
* [R6](/lines/r6.md)
* [R60](/lines/r60.md)
