
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# BCN-Pg. Gràcia

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Renfe](/transports/renfe.md)

## Línies
    
* [R2](/lines/r2.md)
* [R2N](/lines/r2n.md)
* [R2S](/lines/r2s.md)
* [R11](/lines/r11.md)
* [R13](/lines/r13.md)
* [R14](/lines/r14.md)
* [R15](/lines/r15.md)
* [R16](/lines/r16.md)
