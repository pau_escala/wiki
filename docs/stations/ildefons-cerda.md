
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Ildefons Cerdà

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [FGC](/transports/fgc.md)

## Línies
    
* [L8](/lines/l8.md)
* [S3](/lines/s3.md)
* [S4](/lines/s4.md)
* [S8](/lines/s8.md)
* [S9](/lines/s9.md)
* [R5](/lines/r5.md)
* [R50](/lines/r50.md)
* [R6](/lines/r6.md)
* [R60](/lines/r60.md)
