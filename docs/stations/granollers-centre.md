
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Granollers Centre

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Renfe](/transports/renfe.md)

## Línies
    
* [R2](/lines/r2.md)
* [R2N](/lines/r2n.md)
* [R8](/lines/r8.md)
* [R8](/lines/r8.md)
* [R11](/lines/r11.md)
