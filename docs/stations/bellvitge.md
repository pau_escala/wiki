
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Bellvitge

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Metro](/transports/metro.md)

## Línies
    
* [R2](/lines/r2.md)
* [R2N](/lines/r2n.md)
* [R2S](/lines/r2s.md)
* [R15](/lines/r15.md)
* [L1](/lines/l1.md)
