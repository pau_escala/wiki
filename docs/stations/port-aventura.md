
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Port Aventura

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Renfe](/transports/renfe.md)

## Línies
    
* [R16](/lines/r16.md)
