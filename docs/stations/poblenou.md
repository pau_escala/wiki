
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)


# Poblenou

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

* [Metro](/transports/metro.md)

## Línies
    
* [L4](/lines/l4.md)
