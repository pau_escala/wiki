# Informació sobre dades recollides

Diàriament es publiquen alertes a la plataforma, amb informació que pot ser útil per a pal·liar els efectes del trastorn Memetro.

A continuació es poden consultar alguns resultats sobre aquestes dades.

L'estudi està incomplert, sentiu-vos lliures d'afegir qualsevol informació o dada que trobeu adient.



## Índex

1. Número d’alertes per setmana i per dia
2. Número d’alertes publicades per dia de la setmana
3. Hores en que es produeixen les alertes
4. Diferències entre alertes publicades i incidències reals
5. Número d’incidències per estació

* Sobre els scripts utilitzats

  

## 1. Número d’alertes per setmana i per dia

A la Imatge 1, entre les setmanes 18.2022 i 22.2022 s’observa un augment molt més ràpid del número d’alertes, continuat pel que sembla una tornada a la «normalitat».  Amb un extraordinari pic de 292 alertes durant la setmana 21.2022,  compresa entre el 23.05.2022 al 29.05.2022.

Aquest pic succeeix de manera simultània amb l’anunci que TMB feia, dient que hi haurien més controls contra el frau durant quest període. 

En canvi, durant les setmanes 35-37 també s'han fet anuncis d'una campanya en contra del frau però no s'ha detectat cap increment en el número de Mosquits detectats, més enllà d'un petit. Pot ser degut al final de les vacances d'Agost.

<img src="./pictures/alertes_per_setmana.png" style="zoom:50%;" />

​																		**Imatge** **1**: Número d’incidències publicades per setmana.

En la següent imatge (Imatge 2), es pot observar que fins i tot en els períodes on més alertes es publiquen, es poden trobar dies amb molt poques incidències reportades, arribant a zero en ocasions.

<img src="./pictures/alertes_per_dia.png"  />

​																		**Imatge** **2**: Número d’incidències publicades per dia.





## 2. Número d’alertes publicades per dia de la setmana

A la següent Imatge 3 s’observa que el número d’alertes publicades durant el cap de setmana és molt inferior al d’alertes publicades durant els altres dies de la setmana. 

En dissabtes o diumenges normalment es registren només entre 0 i 3 alertes de mosquits per dia.



També s’observa que el número de «Via Lliure» publicades és més gran e dissabte que en diumenge, indicant, pot ser, que més gent viatge en transport públic els dissabtes que els diumenges, tot i que podria ser una simple casualitat.

<img src="./pictures/alertes_per_dia_de_la_setmana.png"  />

​													**Imatge 3:** Número d’incidències publicades segons el dia de la setmana.

Sembla ser, que en caps de setmana és poc probable trobar Mosquits





## 3. Hores en que es produeixen les alertes

En la següent Imatge 5 s’observa a quines hores s’han produït les alertes publicades.

Sembla ser que després de les 22h i abans de les 7h, mai es troben Mosquits.

<img src="./pictures/alertes_per_franja_horaria.png"  />

​														**Imatge 4:** Número d’incidències publicades per franja horària del dia.

## 4. Diferències entre alertes publicades i incidències reals

Les dades presentades per ara son molt primitives, ens donen poca informació. Degut a que un mateix control de Mosquits pot ser registrat molts cops, i semblar que els mosquits han tingut un dia molt actiu, o pocs, i semblar que aquell dia hi ha hagut menys activitat.

Per aquest motiu cal diferenciar quines alertes formen part d’una mateixa incidència (d’un mateix control), i quines formen part d’un control diferent.

Aquesta de per si és una tasca complexa, ja que en ocasions els controls es munten i desmunten i es tornen a muntar uns minuts més tard... A vegades la mateixa comitiva de Mosquits canvia d’estació. I a vegades ens falten dades per a confirmar si un control a desaparegut o si segueix present.



Estudiant els controls de Mosquits, i no les alertes es poden intentar esbrinar més patrons de comportament dels Mosquits de l’AMB.

*Inacabat.*

## 5. Número d’incidències per estació

*Inacabat.*



## 6. 



## Sobre els scripts utilitzats

Els gràfics que es mostren en aquest document s'han realitzat utilitzant dos scripts: 

[html2json](./scripts/colatbot_html2json.md) <--Per a transformar les dades a un format més amigable per a ordinadors
[Estadística](./scripts/colatbot_estadistica.md) <-- Per a extraure informació d'aquestes dades

Per a utilitzar-los primer cal descarregar les dades que es volen analitzar. La forma de fer-ho és anar al canal de Telegram [https://t.me/MemetroBCN](https://t.me/MemetroBCN). Des d'allà, clickar en "Export chat history".

Un cop tenim les dades descarregades en format .html, cal copiar els scripts a la carpeta on ja tenim les dades.

Aleshores es poden executar els scripts:

```bash
python3 colatbot_html2json.py && python3 colatbot_estadistica.py
```

El primer només cal executar-ho una vegada, cada cop que es descarreguen noves dades.
