# Script per a formatar les dades extretes a format .json

```python
#Script utilitzat per extreure la informació dels avisos publicats a t.me/AvisosControlsBCNCanal
#Per a fer servir el programa:
    #Des de l'aplicació de Telegram exporta tots els missatges al teu ordinador
    #Copia els fitxers *.html a la mateixa carpeta on es troba aquest Script
    #Executa el programa: python3 colatbot_html2json.py
    #Per cada arxiu .html el programa crea un .json.

from os import listdir
import json

alerta_colatbot = {
"dia": "dd.MM.aaaa",
"hora": "hh:mm:ss",
"tipus": "Animal",
"transport": "Bicicleta",
"linea": "L0",
"parada": "Ajuntament",
"observacions": "AAAA"
}

#Crea una llista de tots els fitxers amb .html que es troben al work-directory
input_files = [file for file in listdir(".") if ".html" in file]

#Per cada fitxer .html:
for file in input_files:
    output_file = file[:-5]+".json" #Nom del fitxer de sortida
    llista_avisos = [] #Llista de tots els avisos d'un fitxer
    num_avisos = 0 #Comptador d'avisos registrats al fitxer
    bool_Observacions = False #Indicador de si una alerta te o no observacions publicades

    with open(file) as file:
        lines = file.readlines() #Llegeix tot el document
        for i in range(len(lines)): #Per cada línea del fitxer
           #Busca la línea que marca l'inici d'un missatge d'alerta:
            if "<div class=\"pull_right date details\" title=\"" in lines[i]:
                num_avisos += 1 #Incrementa el comptador d'alertes
                alerta_colatbot["dia"] = lines[i].split()[4][7:] #Guarda la data de l'alerta
                alerta_colatbot["hora"] = lines[i].split()[5][:8] #Guarda l'hora de l'alerta

            #Busca la següent línea amb informació rellevant
            if "⏱" in lines[i]: #La línea que busquem sempre conté el caràcter "⏱". Cap altre línea conte aquest caràcter

                #Tenim dos formats: #A partir del 14.05.2022 totes les alertes fan servir el format nou
                                    #Totes les alertes anteriors fan servir el format vell

                #Transforma la data de l'alerta actual en un int
                #Aquest int te la propietat de que sempre augmenta a mida que pasen els dies.
                data_alerta = alerta_colatbot["dia"].split(".")
                data_X = int(data_alerta[0]) + int(data_alerta[1])*100 + int(data_alerta[2])*10000

                if data_X > 20220513: #Si l'alerta actual s'ha publicat després de canvi de format de les publicacions:
                    #Alerta publicada en format nou
                    info = lines[i].split()

                    alerta_colatbot["tipus"] = info[1]

                    alerta_colatbot["transport"] = info[6]

                    alerta_colatbot["linea"] = info[8][:-1]
                    if info[6] == "Metro":
                        alerta_colatbot["linea"] = alerta_colatbot["linea"][1:] #Treu el quadrat de color de les alertes del Metro

                    #Guarda el nom de sencer de la parada
                    alerta_colatbot["parada"] = ""
                    for paraula in range(9, len(info)): #El nom de la parada sempre comença en la string 9
                        if "<br>🔎" in info[paraula]: #Final del nom de parada detectat
                            alerta_colatbot["parada"] = alerta_colatbot["parada"] +" "+ info[paraula][:-5]
                            bool_Observacions = True #Aquesta alerta te observacions escrites
                            break
                        alerta_colatbot["parada"] = alerta_colatbot["parada"] +" "+ info[paraula]
                    alerta_colatbot["parada"] = alerta_colatbot["parada"][1:] #Elimina el primer espai del comentari

                    alerta_colatbot["observacions"] = ""
                    if bool_Observacions: #Si l'alerta te observacions
                        for segment in range(paraula+1, len(info)):
                            alerta_colatbot["observacions"] = alerta_colatbot["observacions"]+" "+info[segment]
                        alerta_colatbot["observacions"] = alerta_colatbot["observacions"][1:] #Elimina el primer espai del comentari

                else:
                    #Alerta publicada en format vell
                    info = lines[i].split()

                    alerta_colatbot["tipus"] = info[8]

                    alerta_colatbot["transport"] = info[11]

                    alerta_colatbot["linea"] = info[13][:-1]
                    if (info[11] == "Metro") and (info[13][0] != "L"):
                        alerta_colatbot["linea"] = alerta_colatbot["linea"][1:] #Treu el quadrat de color de les alertes del Metro

                    #Guarda el nom de sencer de la parada
                    alerta_colatbot["parada"] = ""
                    for paraula in range(14, len(info)): #El nom de la parada sempre comença en la string 13
                        if "<br>🔎" in info[paraula]: #Final del nom de parada detectat
                            alerta_colatbot["parada"] = alerta_colatbot["parada"] +" "+ info[paraula][:-5]
                            bool_Observacions = True #Aquesta alerta te observacions escrites
                            break
                        alerta_colatbot["parada"] = alerta_colatbot["parada"] +" "+ info[paraula]
                    alerta_colatbot["parada"] = alerta_colatbot["parada"][1:] #Elimina el primer espai del comentari

                    alerta_colatbot["observacions"] = ""
                    if bool_Observacions: #Si l'alerta te observacions
                        for segment in range(paraula+1, len(info)):
                            alerta_colatbot["observacions"] = alerta_colatbot["observacions"]+" "+info[segment]
                        alerta_colatbot["observacions"] = alerta_colatbot["observacions"][1:] #Elimina el primer espai del comentari




                #Diccionari completat per aquesta alerta, ara cal afegir-lo a la llista
                llista_avisos.append(alerta_colatbot.copy())

        #Per ultim, guardar la llista en format .json
        #Crea un fitxer buit, on es guardaran les alertas en format .json
        with open(output_file, 'w', encoding='utf-8') as outfile:
            json.dump(llista_avisos, outfile, ensure_ascii=False, indent=4)

        print(num_avisos, "avisos processats al fitxer", output_file)
```

