# Cola't tu també Wiki 

Hola, visitant! 👋

Aquesta wiki pretén aglutinar informació interessant relacionada 
amb l'ús del transport públic sense bitllet!

Podràs trobar informació sobre estacions, línies, consells per entrar 
sense pagar i altra informació útil. 

Aquesta wiki la fem entre totes! És un recurs de suport mutu que hem de construir.

## Com Contribuir

Si tens alguna informació per aportar, vols corregir alguna falta, 
o simplement vols aprendre com modifica-la, dona una ullada a la pàgina:

* [Com contribuir](contribute.md)



## Entrades actuals

* [Estadística sobre els controls de Mosquits ](./analisis_dades/README.md)
* 
