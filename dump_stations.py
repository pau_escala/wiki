import json
import os
import unicodedata
import re
from string import Template

file = open("_data/transports.json", "r")
# file = open("../docs/transports.json", "r")
transports = json.load(file)

transports_raw_dict = transports.copy()

# Transports
def create_transports(raw_t):
    transport_dicts = list()
    transport_refs = dict()
    pk = 1
    for t in raw_t:
        transport_dicts.append({
            "model": "api.transport",
            "pk": pk,
            "fields": {
                "name": t,
            }
        })
        transport_refs[t] = pk
        pk = pk + 1
    return transport_dicts, transport_refs
    # TODO: Create file


raw_transport_names = transports_raw_dict.keys()
transport_dicts, transport_refs = create_transports(raw_transport_names)

# Station

def generate_dicts(t_raw_dict):
    station_to_transport_dicts = dict()
    station_to_lines_dict = dict()

    transport_to_stations_dicts = dict().fromkeys(t_raw_dict.keys())
    transport_to_lines_dicts = dict().fromkeys(t_raw_dict.keys())

    lines_to_transport_dicts = dict()
    lines_to_stations_dicts = dict()

    stations_name_list = set()

    for trans, t_values in t_raw_dict.items():
        lines = t_values["lines"]
        for line, l_values in lines.items():
            if not line in lines_to_transport_dicts.keys():
                lines_to_transport_dicts[line] = list()
                lines_to_stations_dicts[line] = list()
            if transport_to_lines_dicts[trans] is None:
                transport_to_lines_dicts[trans] = list()

            lines_to_transport_dicts[line] = trans
            transport_to_lines_dicts[trans].append(line)

            if lines[line]:
                stations_name_list.update(lines[line]["stops"])

                for stop in lines[line]["stops"]:
                    if not stop in station_to_lines_dict.keys():
                        station_to_lines_dict[stop] = list()
                    station_to_lines_dict[stop].append(line)
                    station_to_transport_dicts[stop] = trans
                    if transport_to_stations_dicts[trans] is None:
                        transport_to_stations_dicts[trans] = list()
                    transport_to_stations_dicts[trans].append(stop)
                    lines_to_stations_dicts[line].append(stop)

    return \
        station_to_transport_dicts, \
        transport_to_lines_dicts, \
        transport_to_stations_dicts,\
        lines_to_transport_dicts,\
        lines_to_stations_dicts, \
        station_to_lines_dict


station_to_transport_dicts, transport_to_lines_dicts, transport_to_stations_dicts, lines_to_transport_dicts, lines_to_stations_dicts, station_to_lines_dict \
    = generate_dicts(transports_raw_dict)


print("AMB Stations...")
print("---------------")

# Print all stations ordered by transport and the lines it belongs to
# for trans, t_values in transports_raw_dict.items():
#     print("#### " + trans)
#     for station, station_transport in station_to_transport_dicts.items():
#         if station_transport is trans:
#             print(station + ":")
#             print("("+", ".join(s for s in station_to_lines_dict[station]) + ")")
#             print("\n")

base_path = "docs/"

transports_folder = "transports/"
lines_folder = "lines/"
stations_folder = "stations/"


def docsify_markdown_link(text, path):
    return "[" + text + "](/" + path + ")"


def get_full_path(element, path):
    return path + sanitize_filename(element) + ".md"


def list_element(element, path):
    return "* " + docsify_markdown_link(element, get_full_path(element, path))


def list_of_links(list: list, path):
    if list is not None:
        return "\n".join(
            list_element(element, path)
            for element in list)


def sanitize_filename(value, allow_unicode=False):
        """
        Taken from https://github.com/django/django/blob/master/django/utils/text.py
        Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
        dashes to single dashes. Remove characters that aren't alphanumerics,
        underscores, or hyphens. Convert to lowercase. Also strip leading and
        trailing whitespace, dashes, and underscores.
        """
        value = str(value)
        if allow_unicode:
            value = unicodedata.normalize('NFKC', value)
        else:
            value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
        value = re.sub(r'[^\w\s-]', '', value.lower())
        return re.sub(r'[-\s]+', '-', value).strip('-_')


def write_file(data, fname):
    path = base_path + fname
    finalData = contribute_page + data

    print("Writing: " + path)
    os.makedirs(os.path.dirname(path), exist_ok=True)
    f = open(path, "w+")
    f.write(finalData)
    f.close()


contribute_page = """
# Aquesta pàgina encara no te dades!

Vols contribuïr a la Wiki?

[Descobreix com!](contribute.md)

"""

def create_transports_files():
    template = Template("""
# $title

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 
    
## Línies
    
$lines_list
    
## Estacions
    
$stations_list
""")

    for t in transport_to_lines_dicts.keys():
        data = {
            'title' : t,
            'lines_list': list_of_links(transport_to_lines_dicts[t], lines_folder),
            'stations_list': list_of_links(transport_to_stations_dicts[t], stations_folder),
        }
        filepath = get_full_path(t, transports_folder)
        filedata = template.substitute(data)
        write_file(filedata, filepath)

    # Create root README.md page
    content = "# Transports\n\n"
    content += list_of_links(transport_to_lines_dicts.keys(), transports_folder)
    write_file(content, transports_folder + "/README.md")


def create_lines_files():
    template = Template("""
# Línia $title

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: sempre hi ha revisors a partir de X parada, 
a l'estiu posen controls, etc... 

## Transport

$transport

## Estacions

$stations_list
""")

    for line, transport in lines_to_transport_dicts.items():
        data = {
            'title': line,
            'transport': list_element(transport, transports_folder),
            'stations_list': list_of_links(lines_to_stations_dicts[line], stations_folder),
        }
        filepath = get_full_path(line, lines_folder)
        filedata = template.substitute(data)
        write_file(filedata, filepath)

    # Create root README.md page
    content = "# Línies\n\n"
    content += list_of_links(lines_to_transport_dicts.keys(), lines_folder)
    write_file(content, lines_folder + "/README.md")


def create_stations_files():
    template = Template("""
# $title

## Anotacions

Aquí es poden escriure trucs o anotacions varies, tipus: 

- Tipus de parada (baixador, parada...)
- Accés a l'entrar
- Accés al sortir
- Te accés lliure
- ...

## Transport

$transport

## Línies
    
$lines_list
""")

    for station, transport in station_to_transport_dicts.items():
        data = {
            'title': station,
            'transport': list_element(transport, transports_folder),
            'lines_list': list_of_links(station_to_lines_dict[station], lines_folder),
        }
        filepath = get_full_path(station, stations_folder)
        filedata = template.substitute(data)
        write_file(filedata, filepath)

    # Create root README.md page
    content = "# Estacions\n\n"
    content += list_of_links(station_to_lines_dict.keys(), stations_folder)
    write_file(content, stations_folder + "/README.md")


create_transports_files()
create_lines_files()
create_stations_files()

